pragma Warnings (Off);
with Ada.Text_IO;
with System.BB.Execution_Time;
pragma Warnings (On);

with System.BB.Protection;
with System.BB.Threads.Queues;

with Core_Execution_Modes;
with Experiment_Info;

package body CPU_Budget_Monitor is

   Hyperperiod_Passed_First_Time : array (System.Multiprocessors.CPU)
                                               of Boolean := (others => False);

   -----------------------
   --  CPU_BE_Detected  --
   -----------------------

   procedure CPU_BE_Detected (E : in out Timing_Event) is
      use System.BB.Threads;
      use System.BB.Threads.Queues;
      use Core_Execution_Modes;
      --  use System.BB.Time;
      pragma Unreferenced (E);
      Self_Id : constant Thread_Id := Running_Thread;
      Task_Exceeded : constant System.Priority :=
                    Self_Id.Data_Concerning_Migration.Id;
   begin
      System.BB.Protection.Enter_Kernel;

      --  Log CPU_Budget_Exceeded
      Self_Id.Log_Table.Times_BE := Self_Id.Log_Table.Times_BE + 1;
      --  Ada.Text_IO.Put ("CPU_" & System.Multiprocessors.CPU'Image (CPU_Id)
      --                          & ": task " & Integer'Image (Task_Exceeded));

      Experiment_Is_Not_Valid := True;
      Guilty_Task := Task_Exceeded;
      Set_Parameters_Referee (False, Experiment_Is_Not_Valid, False);

      System.BB.Protection.Leave_Kernel;
      --  Ada.Text_IO.Put_Line ("BE HANDLED");
   end CPU_BE_Detected;

   --  return True iff we have detected the passage of the hyperperiod
   --  for at least once. It is useful in order to stop logging CPU's Idle_Time
   --  as soon as its hyperperiod expires.
   function Hyperperiod_Not_Yet_Passed
     (CPU_Id : System.Multiprocessors.CPU;
      Now : System.BB.Time.Time) return Boolean;
   pragma Unreferenced (Hyperperiod_Not_Yet_Passed);

   ---------------------
   --  Start_Monitor  --
   ---------------------

   procedure Start_Monitor (For_Time : System.BB.Time.Time_Span) is
      use Real_Time_No_Elab;
      --  use System.BB.Board_Support.Multiprocessors;
      use System.BB.Threads;
      use System.BB.Threads.Queues;
      use Core_Execution_Modes;
      use System.Multiprocessors;
      Now : constant Time := Clock;
      Self_Id : constant Thread_Id := Running_Thread;
      CPU_Id : constant CPU := Self_Id.Active_CPU;
      Cancelled : Boolean;
      pragma Unreferenced (Cancelled);
      --  Task_Exceeded : constant System.Priority := Self_Id.Base_Priority;
   begin

      --  Log that CPU_Id is no longer idle.
      if CPU_Log_Table (CPU_Id).Is_Idle
         --  and Hyperperiod_Not_Yet_Passed (CPU_Id, Now)
      then
         CPU_Log_Table (CPU_Id).Is_Idle := False;

         CPU_Log_Table (CPU_Id).Idle_Time :=
                     CPU_Log_Table (CPU_Id).Idle_Time +
            (Now - CPU_Log_Table (CPU_Id).Last_Time_Idle);
      end if;

      --  Log that thread is (again) on this CPU
      if CPU_Id = CPU'First then
         Executions (Self_Id.Data_Concerning_Migration.Id).Times_On_First_CPU
           := Executions (Self_Id.Data_Concerning_Migration.Id).
                                                  Times_On_First_CPU + 1;
      else
         Executions (Self_Id.Data_Concerning_Migration.Id).Times_On_Second_CPU
           := Executions (Self_Id.Data_Concerning_Migration.Id).
                                                  Times_On_Second_CPU + 1;
      end if;

      Set_Handler
            (Event =>
                BE_Happened (CPU_Id),
            At_Time =>
                For_Time + Real_Time_No_Elab.Clock,
            Handler =>
                CPU_BE_Detected'Access);

      --  Self_Id.T_Start := System.BB.Time.Clock;

   end Start_Monitor;

   ---------------------
   --  Clear_Monitor  --
   ---------------------

   procedure Clear_Monitor (Cancelled : out Boolean) is
      --  use System.BB.Board_Support.Multiprocessors;
      use System.BB.Threads;
      use System.BB.Time;
      use System.BB.Threads.Queues;
      Self_Id : constant Thread_Id := Running_Thread;
      CPU_Id : constant System.Multiprocessors.CPU :=
                                                      Self_Id.Active_CPU;
   begin
      --  Self_Id.T_Clear := System.BB.Time.Clock;

      Cancel_Handler (BE_Happened (CPU_Id), Cancelled);

      if Self_Id.Is_Monitored and Self_Id.State = Runnable then
         --  Ada.Text_IO.Put (Integer'Image (Self_Id.Base_Priority)
         --  & " consumed" & Duration'Image
         --  (System.BB.Time.To_Duration (Self_Id.Active_Budget)) & " => ");
         Self_Id.Active_Budget :=
           Self_Id.Active_Budget - (Self_Id.T_Clear - Self_Id.T_Start);

         --  Ada.Text_IO.Put_Line (Duration'Image (System.BB.Time.To_Duration
         --                                    (Self_Id.Active_Budget)));
      end if;
   end Clear_Monitor;

   ----------------------------------
   --  Hyperperiod_Not_Yet_Passed  --
   ----------------------------------

   --  return True iff we have detected the passage of the hyperperiod
   --  for at least once. It is useful in order to stop logging CPU's Idle_Time
   --  as soon as its hyperperiod expires.

   function Hyperperiod_Not_Yet_Passed
     (CPU_Id : System.Multiprocessors.CPU;
      Now : System.BB.Time.Time) return Boolean is
      use Real_Time_No_Elab;
      Absolute_Hyperperiod : constant Real_Time_No_Elab.Time :=
         Experiment_Info.Get_Parameters.Absolutes_Hyperperiods (CPU_Id);
   begin
      if (not Hyperperiod_Passed_First_Time (CPU_Id))
        and
         Real_Time_No_Elab.">=" (Now, Absolute_Hyperperiod)
      then
         Hyperperiod_Passed_First_Time (CPU_Id) := True;
         --  Ada.Text_IO.Put_Line
         --  ("Stop time log on: " & System.Multiprocessors.CPU'Image (CPU_Id))
      end if;

      return not Hyperperiod_Passed_First_Time (CPU_Id);
   end Hyperperiod_Not_Yet_Passed;

end CPU_Budget_Monitor;
